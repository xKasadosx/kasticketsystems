
import subprocess
import smtplib, os
import socket
import os
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.utils import formatdate
from email import encoders
import threading
import time
import pymysql
import imaplib
import email
import threading
import html2text
import KasTicketConfigvariables as KasTicketConfigvariables
import uuid

config = KasTicketConfigvariables.get_config()
mailUser = config[0]
mailAdress = config[1]
mailPassword = config[2]
mailServerSMTP = config[3]
mailPortSMTP = config[4]
mailServerIMAP = config[5]
mailPortIMAP = config[6]
databaseServer = config[7]
databaseDB = config[8]
databaseUser = config[9]
databasePassword = config[10]
supportMail = config[13]

def trigger():
	while True:
		time.sleep(10)
		check_for_new_tickets()

thread = threading.Thread(target=trigger)
thread.daemon = True
thread.start()

def create_ticket_tracking(ticketID):
    ticket = get_all_ticket_information(ticketID)
    ticketID = ticket[0][0]
    ticketMail = ticket[0][1]
    if(ticket[0][1] != ""):
        ticketSubject = ""
    else:
        ticketSubject = ticket[0][2]
    ticketStatus = ticket[0][6]
    ticketOpened = ticket[0][7]
    ticketHash = ticket[0][12]
    ticketMail = ticketMail.split("@")
    ticketMail = ticketMail[0].split(".")
    
    os.system("touch /var/www/html/kasticket/" + str(ticketHash)+ ".html")
    file = open("/var/www/html/kasticket/"+str(ticketHash)+".html", 'w')
    
    ticketIDHTML =  "<h2>Ticket ID: " + str(ticketID)
    ticketClientHTML = "</h2><br>Ticket ge&ouml;ffnet von: " + str(ticketMail[0]) + " " +str(ticketMail[1])
    ticketSubjectHTML = "<br>Betreff: " + str(ticketSubject) 
    ticketOpenedHTML = "<br>Er&ouml;ffnet am: " + str(ticketOpened)
    ticketStatusHTML = "<br>Ticket Status: Offen"
    ticketSupporterHTML = "<br>Bearbeiter: " 
    ticketPriorityHTML = "<br>Priorit&auml;t: "
    ticketSupportInfoHTML = "<br>Info: " 
    ticketContent = ticketIDHTML + ticketClientHTML +ticketSubjectHTML + ticketOpenedHTML +ticketStatusHTML+ ticketSupporterHTML + ticketPriorityHTML + ticketSupportInfoHTML
    
    
    file.write(ticketContent)
    file.close
    
    ticketTracking = "http://" +str(config[7]) + "/kasticket/"+str(ticketHash)+".html"
    return ticketTracking




def check_for_new_tickets():
    mail  = imaplib.IMAP4_SSL(mailServerIMAP)
    (statusCode, capabilities) = mail.login(mailUser, mailPassword)
    mail.list()
    mail.select('inbox')

    (statusCode, ticketInbox) = mail.search(None, '(UNSEEN)')
    if statusCode == 'OK':
        for ticket in ticketInbox[0].split() :
            typ, ticketData = mail.fetch(ticket,'(RFC822)')

            for data in ticketData:
                if isinstance(data, tuple):
                 
                    ticketContent = ticketData[0][1]

                    rawTicketContent = ticketContent.decode('iso8859-1')
                    ticketContent = email.message_from_string(rawTicketContent)
                    for contentText in ticketContent.walk():
                        if contentText.get_content_type() == "text/html":
                            content = contentText.get_payload(decode=True)
                            content = content.decode('iso8859-1')
                            
                            ticketContent = html_to_text_parser(content)
                    
                    typ, ticketData = mail.store(ticket,'+FLAGS','\\Seen')

                    headerData = email.message_from_bytes(data[1])
                    
                    clientAdress = headerData['From']
                    clientAdress = email.utils.parseaddr(clientAdress)
                    clientAdress = clientAdress[1]
                    ticketSendTO = headerData['To']
                    ticketHeader = headerData['Subject']
                    
                    ticketTimestamp = headerData['Date']
                    ticketTimestamp = email.utils.parsedate(ticketTimestamp)
                    ticketTimestamp = str(ticketTimestamp[0]) + "." + str(ticketTimestamp[1]) + "." + str(ticketTimestamp[2]) + " " + str(ticketTimestamp[3]) + ":" + str(ticketTimestamp[4]) + ":" + str(ticketTimestamp[5])
                    check_for_new_or_old_ticket(ticketTimestamp,clientAdress,ticketHeader,ticketContent,ticketSendTO)


def html_to_text_parser(content):
    parser = html2text.HTML2Text()
    parser.ignore_links = True
    return parser.handle(content)

def check_for_new_or_old_ticket(timestamp,client,header,message,ticketSendTO):
    splitHeader = header.split("|")
    if("Ticket" in splitHeader[0] and find_header(splitHeader[1],splitHeader[2],client) == True):
        ticketID = splitHeader[1]
        messageAnswer = message.split("**Von:**")
        message = messageAnswer[0]
        new_answer(ticketID,message,client,ticketSendTO,timestamp)        
    else:
        new_ticket(client,header,message,ticketSendTO,timestamp)    

#CONNECT TO DATABASE4
def connect_to_database():
    global dbconnect
    dbconnect = pymysql.connect(
        host = databaseServer,
        db = databaseDB, 
        user = databaseUser,
        passwd = databasePassword, 
    )

def new_answer(ticketID,message,sendFrom,sendTo,timestamp):
    try:
        connect_to_database()
        sqlInsertMessage = "INSERT INTO messages (messageID, ticketID, mailID, messageFrom,messageTO, message, timestamp) VALUES(NULL, %s, 0, '%s','%s','%s', '%s')"%(ticketID,sendFrom,sendTo,message,timestamp) 
        insert = dbconnect.cursor()
        insert.execute(sqlInsertMessage)
        dbconnect.commit()  

        connect_to_database()  
        sqlUpdate = "update ticket set newMessage = 1 where ticketID = %s" %(ticketID)
        update = dbconnect.cursor()
        update.execute(sqlUpdate)
        dbconnect.commit()

    except (pymysql.Error, pymysql.Warning) as errorMessage:
        print(errorMessage)
        return 0
    finally:
        dbconnect.close


def find_header(ticketID,header,client):
    try:
        connect_to_database()
        sqlQuery = "select header from ticket where ticketID = '%s' and mailadress = '%s' " %(ticketID,client)
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchone()
        if(result != None and header in result):
            return True
        else:
            return False
    except (pymysql.Error, pymysql.Warning) as errorMessage:
            return 0
    finally:
        dbconnect.close

def new_ticket(client,header,message,messageTO,timestamp):
    try:
        uid = uuid.uuid4()
        ticketHash = uid.hex
        connect_to_database()
        sqlInsertTicket = "INSERT INTO ticket (ticketID, mailadress, header, countMessages, supporterName, ticketPriority, ticketStatus, ticketOpened, ticketClosed, supportInfo, newTicket, newMessage,ticketHash) VALUES(NULL, '%s', '%s', '1', '', '', 0, '%s', '0000:00:00 00:00:00', '',1,0,'%s')" %(client,header,timestamp,ticketHash)
        insert = dbconnect.cursor()
        insert.execute(sqlInsertTicket)
        dbconnect.commit()
        sqlQuery = "select ticketID from ticket where ticketOpened = '%s' "%(timestamp) 
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        ticketID = query.fetchone()
        sqlInsertMessage = "INSERT INTO messages (messageID, ticketID, mailID, messageFrom,messageTO ,message, timestamp) VALUES(NULL, %s, 0,'%s','%s','%s', '%s')"%(ticketID[0],client,messageTO,message,timestamp)
        insert = dbconnect.cursor()
        insert.execute(sqlInsertMessage)
        dbconnect.commit()
        ticketTracking = create_ticket_tracking(ticketID[0])
        send_automated_answer(ticketID[0],client,header,ticketTracking)
        send_info_to_supporter(ticketID[0])
        

    except (pymysql.Error, pymysql.Warning) as errorMessage:
            print(errorMessage)
            return 0
    finally:
        dbconnect.close

def get_all_ticket_information(ticketID):
    try:
        connect_to_database()
        sqlQuery = "select * from ticket where ticketID = '%s'"%(ticketID)  
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        ticket = query.fetchall()
        return ticket
        return 
    except (pymysql.Error, pymysql.Warning) as errorMessage:
        print(errorMessage)
        return 0
    finally:
        dbconnect.close





def send_automated_answer(ticketID,receiver,subject,ticketTracking):

    msg = MIMEMultipart()
    msg['Subject'] = 'Ticket|' + str(ticketID) +"|" +str(subject)
    msg['From'] = mailAdress
    msg['To'] = receiver
    
    mailText = "Wir haben Ihr Ticket erhalten.<br>Ihre Ticket ID ist: "+ str(ticketID) + "<br>Ihr Ticket Status können sie sehen unter: <a href='" +str(ticketTracking)+"'>TICKET TRACKING</a><br><br>Dies ist eine automatisch generierte E-Mail.<br><br>Mit freundlichen Grüßen <br><br>EDV-Support"
    msg.attach(MIMEText(mailText, 'html'))

    try:
        server =smtplib.SMTP(mailServerSMTP,mailPortSMTP)
        server.starttls()
        server.login(mailUser, mailPassword)
        text = msg.as_string()
        server.sendmail(mailAdress,receiver,text)
        server.quit()
        return True
    except (socket.gaierror, socket.error, socket.herror, smtplib.SMTPException) as e:
        return False


def send_info_to_supporter(ticketID):

    msg = MIMEMultipart()
    msg['Subject'] = 'Neues Ticket: ' +str(ticketID)
    msg['From'] = mailAdress
    msg['To'] = supportMail
    
    mailText = "Neues Ticket wurde erstellt! <br> TicketID: " + str(ticketID) + "<br><br>Dies ist eine automatisch generierte E-Mail."
    msg.attach(MIMEText(mailText, 'html'))

    try:
        server =smtplib.SMTP(mailServerSMTP,mailPortSMTP)
        server.starttls()
        server.login(mailUser, mailPassword)
        text = msg.as_string()
        server.sendmail(mailAdress,supportMail,text)
        server.quit()
        return True
    except (socket.gaierror, socket.error, socket.herror, smtplib.SMTPException) as e:
        print(e)
        return False



while True:
    time.sleep(1)


