#!/usr/bin/env python3
from tkinter import messagebox
#0)MailServerlogin
#1)Email mit der Gesendet werden soll
#2)Passwort fur den serverlogin
#3)Server IP / Hostname
#4)Server PORT
#5)Datenbank IP
#6)Datenbank Database
#7)Datenbank Table
#8)Datenbank User
#9)Datenbank Password
#10)Font color
#11)Background color

def get_config():
    fh=open('./config.ini')
    line = [line.rstrip() for line in fh.readlines()]
    fh.close()
    config = line   
    
    return config

def write_config(configvar):
    file = open("./config.ini", 'w')
    
    for variable in configvar:
         file.write(variable +"\n")
    
    file.close
    
    messagebox.showinfo("Config", "Eingaben wurden Gespeichert!")

try:
    configvar = get_config()

except:
    messagebox.showinfo("Error", "Fehler: config datei fehlt!")