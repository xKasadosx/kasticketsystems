import subprocess
import smtplib, os
import socket
import os
import sys
import threading
import time
import pymysql
import threading
import KasTicketConfigvariables as KasTicketConfigvariables
import uuid
from datetime import datetime

def trigger():
	while True:
		time.sleep(600)
		update_ticket_status_HTML()

thread = threading.Thread(target=trigger)
thread.daemon = True
thread.start()

config = KasTicketConfigvariables.get_config()
databaseServer = config[7]
databaseDB = config[8]
databaseUser = config[9]
databasePassword = config[10]

def connect_to_database():
    global dbconnect
    dbconnect = pymysql.connect(
        host = databaseServer,
        db = databaseDB, 
        user = databaseUser,
        passwd = databasePassword, 
    )

def get_tickets_last_two_days():
    try:
        connect_to_database()
        sqlQuery = "SELECT * FROM ticket WHERE ticketOpened >= CURDATE() - INTERVAL 10 DAY"  
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        ticket = query.fetchall()
        return ticket
        return 
    except (pymysql.Error, pymysql.Warning) as errorMessage:
        print(errorMessage)
        return 0
    finally:
        dbconnect.close



def update_ticket_status_HTML():
    print("updating")
    lastTickets = get_tickets_last_two_days()
    for ticket in lastTickets:
        print(ticket[0])
        print(ticket[12])
        print("___"*10)

        timestamp = time.strftime("%d.%m.%Y %H:%M:%S")
        ticketID = ticket[0]
        ticketMail = ticket[1]
        ticketSubject = ticket[2]
        ticketSupporter = ticket[4]
        ticketPriority = ticket[5]
        ticketStatus = ticket[6]
        ticketOpened = ticket[7]
        ticketClosed = ticket[8]
        ticketSupportInfo = ticket[9]
        ticketHash = ticket[12]
        ticketMail = ticketMail.split("@")
        ticketMail = ticketMail[0].split(".")

        file = open("/var/www/html/kasticket/"+str(ticket[12])+".html", 'w')
        ticketIDHTML =  "<h2>Ticket ID: " + str(ticketID)
        ticketClientHTML = "</h2><br>Ticket ge&ouml;ffnet von: " + str(ticketMail[0]) + " " +str(ticketMail[1])
        ticketSubjectHTML = "<br>Betreff: " + str(ticketSubject) 
        ticketOpenedHTML = "<br>Er&ouml;ffnet am: " + str(ticketOpened)
        if(ticketStatus == 0):
                ticketStatusHTML = "<br>Ticket Status: Offen"
                ticketClosedHTML = ""
        else:
                ticketStatusHTML = "<br>Ticket Status: Geschlossen"
                ticketClosedHTML = "<br>Ticket geschlossen am: " + str(ticketClosed)
                
        ticketSupporter = "<br>Bearbeiter: " + str(ticketSupporter)
        ticketPriorityHTML = "<br>Priorit&auml;t: " + str(ticketPriority)
        ticketSupportInfoHTML = "<br>Info: " + str(ticketSupportInfo)
        ticketContent = ticketIDHTML + ticketClientHTML +ticketSubjectHTML + ticketOpenedHTML + ticketStatusHTML + ticketClosedHTML + ticketSupporter + ticketPriorityHTML + ticketSupportInfoHTML
        file.write(ticketContent)
        
        file.close
while True:
        time.sleep(1)
