import os
from tkinter import *  
import KasTicketConfigvariables as KasTicketConfigvariables
config = KasTicketConfigvariables.get_config()

configwindow = Tk()
configwindow.title("Config")
config = KasTicketConfigvariables.get_config()
configwindow.configure(background=config[12])  

def quit_window():
    configwindow.destroy()

def save_data():
    config[0] = textMailuser.get()
    config[1] = textMailAdresse.get()
    config[2] = textMailPassword.get()
    config[3] = textMailServerSMTP.get()
    config[4] = textMailPortSMTP.get()
    config[5] = textMailServerIMAP.get()
    config[6] = textMailPortIMAP.get()
    config[7] = textDatabaseIP.get() 
    config[8] = textDatabaseName.get()
    config[9] = textDatabaseUser.get()
    config[10] = textDatabasePassword.get()
    KasTicketConfigvariables.write_config(config)
    KasTicketConfigvariables.configvar = KasTicketConfigvariables.get_config()
    
labelMailuser = Label(configwindow, text="Mailserver user:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=1, column=1)
textMailuser = Entry(configwindow,width=40) 
textMailuser.insert(INSERT,config[0])
textMailuser.grid(row=1, column=2)

labelMailAdresse = Label(configwindow, text="Mailadresse:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=2, column=1)
textMailAdresse = Entry(configwindow,width=40)
textMailAdresse.insert(INSERT,config[1])
textMailAdresse.grid(row=2, column=2)
   
labelMailPassword = Label(configwindow, text="Mailpasswort:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=3, column=1)
textMailPassword = Entry(configwindow,show="*",width=40)
textMailPassword.insert(INSERT,config[2])
textMailPassword.grid(row=3, column=2)

labelMailServerSMTP = Label(configwindow, text="Mailserver SMTP IP:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=4, column=1)
textMailServerSMTP = Entry(configwindow,width=40)
textMailServerSMTP.insert(INSERT,config[3])
textMailServerSMTP.grid(row=4, column=2)

labelMailPortSMTP = Label(configwindow, text="Mailserver SMTP Port:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=5, column=1)
textMailPortSMTP = Entry(configwindow,width=40)
textMailPortSMTP.insert(INSERT,config[4])
textMailPortSMTP.grid(row=5, column=2)

labelMailServerIMAP = Label(configwindow, text="Mailserver IMAP IP:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=6, column=1)
textMailServerIMAP = Entry(configwindow,width=40)
textMailServerIMAP.insert(INSERT,config[5])
textMailServerIMAP.grid(row=6, column=2)

labelMailPortIMAP = Label(configwindow, text="Mailserver IMAP Port:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=7, column=1)
textMailPortIMAP = Entry(configwindow,width=40)
textMailPortIMAP.insert(INSERT,config[6])
textMailPortIMAP.grid(row=7, column=2)

labelDatabaseIP = Label(configwindow, text="Datenbank IP:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=8, column=1)
textDatabaseIP = Entry(configwindow,width=40)
textDatabaseIP.insert(INSERT,config[7])
textDatabaseIP.grid(row=8, column=2)

labelDatabaseName = Label(configwindow, text="Datenbankname:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=9, column=1)
textDatabaseName = Entry(configwindow,width=40)
textDatabaseName.insert(INSERT,config[8])
textDatabaseName.grid(row=9, column=2)

labelDatabaseUser = Label(configwindow, text="Datenbank User:",height=2, width=15, background=config[12],foreground=config[11],font="bold") .grid(row=10, column=1)
textDatabaseUser = Entry(configwindow,width=40)
textDatabaseUser.insert(INSERT,config[9])
textDatabaseUser.grid(row=10, column=2)

labelDatabasePassword = Label(configwindow, text="Datenbank Passwort:",height=2, width=20, background=config[12],foreground=config[11],font="bold") .grid(row=11, column=1)
textDatabasePassword = Entry(configwindow,show="*",width=40)
textDatabasePassword.insert(INSERT,config[10])
textDatabasePassword.grid(row=11, column=2)

freespaceLabel = Label(configwindow, text="",height=2, width=5, background=config[12],foreground=config[11],font="bold") .grid(row=1, column=3)
ButtonSave = Button(configwindow, text="Speichern",height=3, width=20   , background='white', command=save_data) .grid(row=14, column=2)
ButtonClose = Button(configwindow, text="Schließen",height=3, width=20   , background='white', command=quit_window) .grid(row=14, column=1)


configwindow.resizable(False, False) 
configwindow.mainloop()
