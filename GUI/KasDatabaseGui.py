import os
from tkinter import *  
from tkinter import messagebox
from tkinter.scrolledtext import *
from tkinter.simpledialog import askstring
from tkinter.messagebox import showinfo
import KasDatabase as KasDatabase
import KasTicketViewGui as KasTicketViewGui
import KasTicketFunctions as KasTicketFunctions
import time
import KasTicketDatabaseFilterGui as KasTicketDatabaseFilterGui
import KasTicketConfigvariables as KasTicketConfigvariables
config = KasTicketConfigvariables.get_config()

def database_window(window,ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab): # new window definition
    databaseWindow = Toplevel(window)
    databaseWindow.title("Ticket History")
    databaseWindow.configure(background=config[10])
    databaseWindow.resizable(False, False) 

    def initalize_ticket_database():
        KasTicketFunctions.initalize_ticket_database(ticketboxAllTickets)
        
    def open_ticket_from_list(event):
	    selectedTicket = ticketboxAllTickets.selection_get()
	    KasTicketViewGui.view_ticket(window,selectedTicket,ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab)

    def open_filter_window():
        KasTicketDatabaseFilterGui.open_database_filter(databaseWindow,ticketboxAllTickets)

    def quit_window():
        databaseWindow.destroy()

    def clear():
        kasmailfunctions.clear(textCustomerID,textCustomerName,textCustomerMailOne,textCustomerMailTwo,textCustomerMailThree)

    scrollbarAllTickets = Scrollbar(databaseWindow,orient='vertical',background=config[10])
    scrollbarAllTickets.grid(row=1,column=1,rowspan=3, sticky='ens')


    ticketboxAllTickets = Listbox(databaseWindow, width=60, height=10,background=config[10],highlightthickness="0",bd=0,foreground=config[9],font="bold", yscrollcommand=scrollbarAllTickets.set)
    ticketboxAllTickets.grid(row=1,column=2,rowspan=3)
    ticketboxAllTickets.bind('<Double-1>', open_ticket_from_list)
    scrollbarAllTickets.config(command=ticketboxAllTickets.yview)
    scrollbarAllTickets.config(command=ticketboxAllTickets.yview)

    ButtonReload = Button(databaseWindow, text="Reload",height=5, width=15, background='white',font="bold", command=initalize_ticket_database) .grid(row=1, column=4)
    ButtonFilter = Button(databaseWindow, text="Set Filter",height=5, width=15, background='white',font="bold", command=open_filter_window) .grid(row=2, column=4)
    
    KasTicketFunctions.initalize_ticket_database(ticketboxAllTickets)

