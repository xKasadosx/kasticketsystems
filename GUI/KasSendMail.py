#!/usr/bin/env python3

#IMPORT LIBS
import subprocess
import smtplib, os
import socket
import os
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.utils import formatdate
from email import encoders
from tkinter import messagebox
from tkinter import messagebox
import KasDatabase as KasDatabase
import KasTicketConfigvariables as KasTicketConfigvariables

def send_ticket_answer(ticketID,answer):



    senderLocal = KasTicketConfigvariables.configvar[0]
    senderMail = KasTicketConfigvariables.configvar[1]
    password =  KasTicketConfigvariables.configvar[2]
    serverHost = KasTicketConfigvariables.configvar[3]
    serverPort = KasTicketConfigvariables.configvar[4]
    
    receiver,subject = KasDatabase.get_mailadress(ticketID)
    ticketMessages = KasDatabase.get_ticket_content(ticketID,"mail")

    msg = MIMEMultipart()
    subject = 'Ticket|' + str(ticketID) +"|" +str(subject)
    msg['Subject'] = subject
    msg['From'] = senderMail
    msg['To'] = receiver
    mailText = ""

    for message in ticketMessages:
        #build header
        mailSendFrom = "<b>Von:</b> " + str(message[1])+"<br>"
        mailSendTime = "<b>Gesendet:</b> " + str(message[3])+"<br>"
        mailSendTo = "<b>Bis:</b> " + str(message[2]) +"<br>"
        mailSubject = "<b>Betreff:</b> " + str(subject) +"<br><br><br>"
        header = mailSendFrom + mailSendTime + mailSendTo + mailSubject
        
        #\n replace mit <br>
        message = message[0].replace("\n","<br>")
        #packt in mail text alle folgenden messages + timestamp + message

        mailText = str(mailText) + header + str(message) + "<br>" + "_"*60 +"<br>"
    answer = answer.replace("\n","<br>")
    mailText = str(answer) +"<br>"+ "_"*60 + "<br>" + str(mailText) 
    msg.attach(MIMEText(mailText, 'html'))

    try:
        server =smtplib.SMTP(serverHost,serverPort)
        server.starttls()
        server.login(senderLocal, password)
        text = msg.as_string()
        server.sendmail(senderMail,receiver,text)
        server.quit()
        return True
    except (socket.gaierror, socket.error, socket.herror, smtplib.SMTPException) as e:
        messagebox.showinfo("ERROR", "Antwort konnte nicht Gesendet werden! \n\n" + str(e))
        return False
