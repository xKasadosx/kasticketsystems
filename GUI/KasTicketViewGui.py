import os
from tkinter import *  
from tkinter import messagebox
from tkinter.scrolledtext import *
from tkinter.simpledialog import askstring
from tkinter.messagebox import showinfo
import KasDatabase as KasDatabase
import KasSendMail as KasSendMail
import time
import KasTicketFunctions as KasTicketFunctions
import KasTicketConfigvariables as KasTicketConfigvariables
config = KasTicketConfigvariables.get_config()
fontsize = "40"
def view_ticket(window,selectedTicket,ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab):
	cuttedHeader = selectedTicket.split(" ")
	cuttedHeader = cuttedHeader[1]
	cuttedHeader = cuttedHeader.split("|")
	ticketID = cuttedHeader[0]

	activeTicketWindow = Toplevel(window)
	activeTicketWindow.title("Ticket | ID: " + str(ticketID))
	activeTicketWindow.configure(background=config[10])
	#activeTicketWindow.geometry("483x655+500+100")
	activeTicketWindow.resizable(False, False) 

################################# F U N K T I O N S ########################################
	def quit():
		activeTicketWindow.destroy()

	def send_answer():
		message = ticketAnswer.get(1.0,END)
		timestamp = KasTicketFunctions.create_timestamp()
		sendFlag = KasSendMail.send_ticket_answer(ticketID,message)
		if(sendFlag == True):
			KasDatabase.answer_ticket(ticketID,ticketClient,message,timestamp)
			KasTicketFunctions.reload_ticket_content(ticketAnswer,ticketContent,ticketID,labelTicketLastMessage)

	def close_ticket():
		KasTicketFunctions.close_ticket(activeTicketWindow,ticketID)
		KasTicketFunctions.reload_tickets(ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab)

	def update_ticket():
		timestamp = KasTicketFunctions.create_timestamp()
		dataBaseTicketSupporter =   dropdownSupporterText.get()
		databaseTicketPriority = dropdownTicketPriroityText.get()
		KasDatabase.update_ticket(ticketID,dataBaseTicketSupporter,databaseTicketPriority)
		if(dropdownSupporterText.get() != "" and dropdownTicketPriroityText.get() != ""):
			KasDatabase.unmark_new_ticket(ticketID)
		KasTicketFunctions.reload_tickets(ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab)

	ticketContent = ScrolledText(activeTicketWindow,width=60, height=15, wrap='word', background=config[10], foreground=config[9])
	ticketID, ticketClient,ticketHeader,ticketContentText,ticketSupporter,ticketPriority,ticketStatus,ticketOpened,ticketClosed,ticketAdditionalInfo,ticketLastMessageTime = KasTicketFunctions.inital_ticket(ticketID,ticketContent)


	ticketInfoIDandCreated = "TicketID: " + str(ticketID) + " Erstellt: " + str(ticketOpened)
	ticketInfoLastMessageTime = "Letzte Nachricht: " + str(ticketLastMessageTime)
	ticketInfoClosed = "Ticket Geschlossen: " + str(ticketClosed)
	ticketInfoClient = "Client: " + str(ticketClient)
	ticketInfoHeader = "Betreff: " + str(ticketHeader)
	

	labelTicketIDandCreated = Label(activeTicketWindow, text=ticketInfoIDandCreated,anchor=W, justify=LEFT, background=config[10],foreground=config[9], font=("bold"), height=1, width=40) 
	labelTicketIDandCreated.grid(row=1, column=1,sticky='w')

	labelTicketLastMessage = Label(activeTicketWindow, text=ticketInfoLastMessageTime,anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
	labelTicketLastMessage .grid(row=2, column=1,sticky='w')

	labelTicketClosed = Label(activeTicketWindow, text=ticketInfoClosed,anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
	labelTicketClosed.grid(row=3, column=1,sticky='w')

	labelClient = Label(activeTicketWindow, text=ticketInfoClient,anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
	labelClient.grid(row=4, column=1,sticky='w')

	labelHeader = Label(activeTicketWindow, text=ticketInfoHeader,anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
	labelHeader.grid(row=5, column=1,sticky='w')

################################################# T I C K E T I N F O R M A T I O N####################################
	##Supporter##

	if(ticketStatus == 0):
		labelSupporter = Label(activeTicketWindow, text="Bearbeiter: ",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
		labelSupporter.grid(row=6, column=1,sticky='w')

		dropdownSupporterFrame = Frame(activeTicketWindow)
		dropdownSupporterFrame.grid(row=6,column=1)
		dropdownSupporterText = StringVar(activeTicketWindow)
		dropDownSupporterOptions = KasTicketFunctions.get_supporter_names()
		dropdownSupporterText.set(ticketSupporter) # set the default option
		dropdownSupporterFrame.config(bg = "black",width=6)

		popupSupporterMenu = OptionMenu(dropdownSupporterFrame, dropdownSupporterText, *dropDownSupporterOptions)	
		popupSupporterMenu.grid(row = 6, column = 1)
		popupSupporterMenu.config(bg="black",foreground="white",width=6)
	else:
		labelSupporter = Label(activeTicketWindow, text="Bearbeiter war: " +str(ticketSupporter),anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
		labelSupporter.grid(row=6, column=1,sticky='w')
	##Prio###
	if(ticketStatus == 0):
		labelTicketPriority = Label(activeTicketWindow, text="Priorität: ",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
		labelTicketPriority.grid(row=7, column=1,sticky='w')

		dropdownTicketPriority = Frame(activeTicketWindow)
		dropdownTicketPriority.grid(row=7,column=1)
		dropdownTicketPriroityText = StringVar(activeTicketWindow)
		dropDownTicketPriortyOptions = [ 'Hoch','Mittel','Niedrig']
		dropdownTicketPriroityText.set(ticketPriority) # set the default option
		dropdownTicketPriority.config(bg = "black")

		popupTicketPriorityMenu = OptionMenu(dropdownTicketPriority, dropdownTicketPriroityText, *dropDownTicketPriortyOptions)	
		popupTicketPriorityMenu.grid(row = 7, column = 1)
		popupTicketPriorityMenu.config(bg="black",foreground="white",width=6)
	else:
		labelTicketPriority = Label(activeTicketWindow, text="Priorität war: " +str(ticketPriority),anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
		labelTicketPriority.grid(row=7, column=1,sticky='w')
	############################################################################################################################################
	##### if true .. neue text box und erklären was das problem ist, das wird dann in eine datenbank tabelle geschrieben mit lösungs ansätze####
	############################################################################################################################################

##################################### T I C K E T I N H A L T ########################################
	labelTicket = Label(activeTicketWindow, text="Ticket Inhalt",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
	labelTicket.grid(row=8, column=1,sticky='w')
	
	if(ticketStatus ==  0):
		ticketContent.config(width=60, height=15, wrap='word', background=config[10], foreground=config[9])
	else:
		ticketContent.config(width=60, height=15, wrap='word', background=config[10], foreground=config[9])
	ticketContent.insert(1.0, ticketContentText)
	ticketContent.grid(row=9,column=1)
	ticketContent.config(state=DISABLED)

##################################### A N S W E R T I C K E T S#########################################
	if(ticketStatus == 0):
		labelMail = Label(activeTicketWindow, text="Antwort",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
		labelMail.grid(row=10, column=1,sticky='w')

		defaultAnswerText = ' \n \nMit Freundlichen Grüßen,\n\nEDV Support'

		ticketAnswer = ScrolledText(activeTicketWindow,width=60, height=10, wrap='word', background=config[10], foreground=config[9])
		ticketAnswer.insert(1.0, defaultAnswerText)
		ticketAnswer.grid(row=11,column=1)
	else:
		labelSupporterInfo = Label(activeTicketWindow, text="Supporter Info",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=40) 
		labelSupporterInfo.grid(row=10, column=1,sticky='w')
	
		ticketSupporterInfo = ScrolledText(activeTicketWindow,width=60, height=5, wrap='word', background=config[10], foreground=config[9])
		ticketSupporterInfo.insert(1.0, ticketAdditionalInfo)
		ticketSupporterInfo.grid(row=11,column=1)
		ticketSupporterInfo.config(state=DISABLED)

######################################### B U T T O N S ################################################
	if(ticketStatus == 0):
		ButtonUpdate =Button(activeTicketWindow, text="Update", height=2,width=16, background='white',font="bold",command=update_ticket) .grid(row=12, column=1,sticky='e')
		ButtonSend =Button(activeTicketWindow, text="Senden", height=2,width=15, background='white',font="bold",command=send_answer) .grid(row=12, column=1)
		ButtonClose =Button(activeTicketWindow, text="Ticket Abschließen", height=2,width=15, background='white',font="bold",command=close_ticket) .grid(row=12, column=1,sticky='w')
	else:
		ButtonClose =Button(activeTicketWindow, text="Schließen", height=2,width=16, background='white',font="bold",command=quit) .grid(row=12, column=1)
	KasDatabase.mark_ticket_as_read(ticketID)
	KasTicketFunctions.reload_tickets(ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab)
		