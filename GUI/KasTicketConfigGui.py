import os
from tkinter import *  
import KasTicketConfigvariables as KasTicketConfigvariables
config = KasTicketConfigvariables.get_config()

def configuration_window(window): # new window definition
    configwindow = Toplevel(window)
    configwindow.title("Config")
    config = KasTicketConfigvariables.get_config()
    configwindow.configure(background=config[10])
    
    def quit_window():
        configwindow.destroy()

    def save_data():
        config[0] = textMailLogin.get()
        config[1] = textMailAdresse.get()
        config[2] = textMailPassword.get()
        config[3] = textMailIP.get()
        config[4] = textMailPort.get()
        config[5] = textDatabaseIP.get()
        config[6] = textDatabaseName.get()
        config[7] = textDatabaseUser.get()
        config[8] = textDatabasePassword.get()
        config[9] = dropdownTextColorText.get()
        config[10] = dropdownBackgroundColorText.get()
        KasTicketConfigvariables.write_config(config)
        #labelInfoText.config(text="Konfiguration gespeichert", foreground="yellow")
        KasTicketConfigvariables.configvar = KasTicketConfigvariables.get_config()
    
    labelMailLogin = Label(configwindow, text="Mailserver Login:",height=2, width=15, background=config[10],foreground=config[9],font="bold") .grid(row=1, column=1)
    textMailLogin = Entry(configwindow,width=40) 
    textMailLogin.insert(INSERT,config[0])
    textMailLogin.grid(row=1, column=2)

    labelMailAdresse = Label(configwindow, text="Mailadresse:",height=2, width=15, background=config[10],foreground=config[9],font="bold") .grid(row=2, column=1)
    textMailAdresse = Entry(configwindow,width=40)
    textMailAdresse.insert(INSERT,config[1])
    textMailAdresse.grid(row=2, column=2)
    
    labelMailPassword = Label(configwindow, text="Mailpasswort:",height=2, width=15, background=config[10],foreground=config[9],font="bold") .grid(row=3, column=1)
    textMailPassword = Entry(configwindow,show="*",width=40)
    textMailPassword.insert(INSERT,config[2])
    textMailPassword.grid(row=3, column=2)

    labelMailIP = Label(configwindow, text="Mailserver IP:",height=2, width=15, background=config[10],foreground=config[9],font="bold") .grid(row=4, column=1)
    textMailIP = Entry(configwindow,width=40)
    textMailIP.insert(INSERT,config[3])
    textMailIP.grid(row=4, column=2)

    labelMailPort = Label(configwindow, text="Mailserver Port:",height=2, width=15, background=config[10],foreground=config[9],font="bold") .grid(row=5, column=1)
    textMailPort = Entry(configwindow,width=40)
    textMailPort.insert(INSERT,config[4])
    textMailPort.grid(row=5, column=2)

    labelDatabaseIP = Label(configwindow, text="Datenbank IP:",height=2, width=15, background=config[10],foreground=config[9],font="bold") .grid(row=6, column=1)
    textDatabaseIP = Entry(configwindow,width=40)
    textDatabaseIP.insert(INSERT,config[5])
    textDatabaseIP.grid(row=6, column=2)

    labelDatabaseName = Label(configwindow, text="Datenbankname:",height=2, width=15, background=config[10],foreground=config[9],font="bold") .grid(row=7, column=1)
    textDatabaseName = Entry(configwindow,width=40)
    textDatabaseName.insert(INSERT,config[6])
    textDatabaseName.grid(row=7, column=2)

    labelDatabaseUser = Label(configwindow, text="Datenbank User:",height=2, width=15, background=config[10],foreground=config[9],font="bold") .grid(row=8, column=1)
    textDatabaseUser = Entry(configwindow,width=40)
    textDatabaseUser.insert(INSERT,config[7])
    textDatabaseUser.grid(row=8, column=2)

    labelDatabasePassword = Label(configwindow, text="Datenbank Passwort:",height=2, width=20, background=config[10],foreground=config[9],font="bold") .grid(row=9, column=1)
    textDatabasePassword = Entry(configwindow,show="*",width=40)
    textDatabasePassword.insert(INSERT,config[8])
    textDatabasePassword.grid(row=9, column=2)

    labelTextColor = Label(configwindow, text="Schrift Farbe: ", background=config[10],foreground=config[9], font="bold", height=1, width=40)
    labelTextColor.grid(row=10, column=1)
    dropdownTextColorFrame = Frame(configwindow)
    dropdownTextColorFrame.grid(row=10,column=2)
    dropdownTextColorText = StringVar(configwindow)
    dropDownTextColorOptions = ["black","white","red","orange red","orange","yellow orange","yellow","green yellow","green","blue","blue violet","violet","violet red"]
    dropdownTextColorText.set(config[9]) # set the default option
    dropdownTextColorFrame.config(bg = "black")
         
    popupSupporterMenu = OptionMenu(dropdownTextColorFrame, dropdownTextColorText, *dropDownTextColorOptions)
    popupSupporterMenu.grid(row = 10, column = 2)
    popupSupporterMenu.config(bg="white",foreground="black",width=11)
    
    labelBackgroundColor = Label(configwindow, text="Hintergrund Farbe: ", background=config[10],foreground=config[9], font="bold", height=1, width=40)
    labelBackgroundColor.grid(row=11, column=1)
    dropdownBackgroundColorFrame = Frame(configwindow)
    dropdownBackgroundColorFrame.grid(row=11,column=2)
    dropdownBackgroundColorText = StringVar(configwindow)
    dropDownBackgroundColorOptions =  ["black","white","red","orange red","orange","yellow orange","yellow","green yellow","green","blue","blue violet","violet","violet red"]
    dropdownBackgroundColorText.set(config[10]) # set the default option
    dropdownBackgroundColorFrame.config(bg = "black")
    
    popupSupporterMenu = OptionMenu(dropdownBackgroundColorFrame, dropdownBackgroundColorText, *dropDownBackgroundColorOptions)
    popupSupporterMenu.grid(row = 11, column = 2)
    popupSupporterMenu.config(bg="white",foreground="black",width=11)




    freespaceLabel = Label(configwindow, text="",height=2, width=5, background=config[10],foreground=config[9],font="bold") .grid(row=1, column=3)


    ButtonSave = Button(configwindow, text="Speichern",height=3, width=20   , background='white', command=save_data) .grid(row=14, column=2)
    ButtonClose = Button(configwindow, text="Schließen",height=3, width=20   , background='white', command=quit_window) .grid(row=14, column=1)
    #configwindow.geometry("450x530+700+100")
    