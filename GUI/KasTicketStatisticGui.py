#!/usr/bin/env python3
import os
import glob
from tkinter import ttk
from tkinter import *  
from tkinter import filedialog
from datetime import datetime
import KasTicketFunctions as KasTicketFunctions
import KasTicketConfigvariables as KasTicketConfigvariables
import KasDatabase as KasDatabase

def statistic_window(window): # new window definition
    statisticWindow = Toplevel(window)
    statisticWindow.title("Statistik")
    config = KasTicketConfigvariables.get_config()
    statisticWindow.configure(background=config[10])

    
    tabs = ttk.Notebook(statisticWindow)

    todayTab = Frame(tabs)
    yesterdayTab = Frame(tabs)
    twoWeeksTab = Frame(tabs)
    totalTab = Frame(tabs)

    tabs.grid(row=1,column=1)

    tabs.add(todayTab,text="Heute")
    tabs.add(yesterdayTab,text="Gestern")
    tabs.add(twoWeeksTab,text="2 Wochen")
    tabs.add(totalTab,text="Gesamt")

    def quit_window():
        statisticWindow.destroy()

    def inital_statistic():
        statisticOverAll,statisticTwoWeeks,statisticYesterday,statisticToday = KasDatabase.fetch_statistic()
        
        overAllTicktets = "Tickets: " +str(statisticOverAll[0]) + "\n"
        overAllticketsOpened = "Tickets Offen: " +str(statisticOverAll[2]) + "\n"
        overAllticketsClosed = "Tickets geschlossen: " +str(statisticOverAll[1]) + "\n"
        overAllText = overAllTicktets + overAllticketsOpened + overAllticketsClosed
        for data in statisticOverAll:
            if("Tickets" in str(data)):
                overAllText = overAllText + data +"\n"
        labelStatisticTotal.config(text=overAllText) 

        twoWeeksTicktets = "Tickets: " +str(statisticTwoWeeks[0]) + "\n"
        twoWeeksticketsOpened = "Tickets Offen: " +str(statisticTwoWeeks[2]) + "\n"
        twoWeeksticketsClosed = "Tickets geschlossen: " +str(statisticTwoWeeks[1]) + "\n"
        twoWeeksText = twoWeeksTicktets + twoWeeksticketsOpened + twoWeeksticketsClosed
        for data in statisticTwoWeeks:
            if("Tickets" in str(data)):
                
                twoWeeksText = twoWeeksText + data +"\n"
        labelStatisticTwoWeeks.config(text=twoWeeksText) 

      
        yesterdayTicktets = "Tickets: " +str(statisticYesterday[0]) + "\n"
        yesterdayticketsOpened = "Tickets Offen: " +str(statisticYesterday[2]) + "\n"
        yesterdayticketsClosed = "Tickets geschlossen: " +str(statisticYesterday[1]) + "\n"
        yesterdayText = yesterdayTicktets + yesterdayticketsOpened +yesterdayticketsClosed
        for data in statisticYesterday:
            if("Tickets" in str(data)):
                yesterdayText = yesterdayText + data +"\n"
        labelStatisticYesterday.config(text=yesterdayText) 

        todayTicktets = "Tickets: " +str(statisticToday[0]) + "\n"
        todayticketsOpened = "Tickets Offen: " +str(statisticToday[2]) + "\n"
        todayticketsClosed = "Tickets geschlossen: " +str(statisticToday[1]) + "\n"
        todayText = todayTicktets + todayticketsOpened + todayticketsClosed
        for data in statisticToday:
            if("Tickets" in str(data)):
                todayText = todayText + data +"\n"
        labelStatisticToday.config(text=todayText) 
   
    labelStatisticToday = Label(todayTab, text="",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=10, width=40) 
    labelStatisticToday.grid(row=1, column=1,sticky='w')

    labelStatisticYesterday = Label(yesterdayTab, text="",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=10, width=40) 
    labelStatisticYesterday.grid(row=1, column=1,sticky='w')

    labelStatisticTwoWeeks = Label(twoWeeksTab, text="",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=10, width=40) 
    labelStatisticTwoWeeks.grid(row=1, column=1,sticky='w')

    labelStatisticTotal = Label(totalTab, text="",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=10, width=40) 
    labelStatisticTotal.grid(row=1, column=1,sticky='w')
    inital_statistic()