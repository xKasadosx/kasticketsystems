#!/usr/bin/env python3
import pymysql
from tkinter import messagebox

import KasTicketConfigvariables as KasTicketConfigvariables

#CONNECT TO DATABASE4
def connect_to_database():
    global dbconnect
    dbconnect = pymysql.connect(
        host = KasTicketConfigvariables.configvar[5],
        db = KasTicketConfigvariables.configvar[6], 
        user = KasTicketConfigvariables.configvar[7],
        passwd = KasTicketConfigvariables.configvar[8], 
    )
    

def get_all_not_done_tickets():
    try:
        connect_to_database()
        sqlQuery = "select supporterName, ticketID, header, ticketPriority, mailadress,newTicket, newMessage from ticket where ticketStatus = 0 order by ticketPriority asc"
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchall()
        return result
    except:
        return 0
    finally:
        dbconnect.close

def get_mailadress(ticketID):
    try:
        connect_to_database()
        sqlQuery = "select mailadress, header from ticket where ticketID = %s"%(ticketID)
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchone()
        mailadress = result[0]
        subject = result[1]
        return mailadress,subject
    except (pymysql.Error, pymysql.Warning) as errorMessage:
            print(errorMessage)
            return 0
    finally:
        dbconnect.close


##muss noch gut geprüft werden
def get_ticket_content(ticketID,modus):
    try:
        connect_to_database()
        if(modus == "mail"):
            sqlQuery = "select message, messageFrom, messageTo, date_format(timestamp,'%%d.%%m.%%Y %%H:%%i:%%s') from messages where ticketID  = %s order by messageID desc "%(ticketID)
        if(modus == "ticket"):
            sqlQuery = "select message, messageFrom, messageTo, date_format(timestamp,'%%d.%%m.%%Y %%H:%%i:%%s') from messages where ticketID  = %s order by messageID asc "%(ticketID)
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchall()
        return result
    except:
        return 0
    finally:
        dbconnect.close

def get_timestamp_of_last_message(ticketID):
    try:
        connect_to_database()
        sqlQuery = "select date_format(timestamp,'%%d.%%m.%%Y %%H:%%i:%%s') from messages where ticketID = %s order by messageID desc"%(ticketID)
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchone()
        return result
    except (pymysql.Error, pymysql.Warning) as errorMessage:
            print(errorMessage)
            return 0
    finally:
        dbconnect.close

def get_all_tickets():
    try:
        connect_to_database()
        #####################0         1         2            3            4              5
        sqlQuery = "select ticketID, mailadress ,header, supporterName,ticketStatus, ticketPriority from ticket"
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchall()
        return result

    except:
        return 0
    finally:
        dbconnect.close

##done
def get_ticket_information(ticketID):
    try:
        connect_to_database()
        sqlQuery = "select * from ticket where ticketID = " + ticketID
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchone()
        return result

    except:
        return 0
    finally:
        dbconnect.close
def unmark_new_ticket(ticketID):
    try:
        connect_to_database()  
        sqlUpdate = "update ticket set newTicket = 0 where ticketID = %s" %(ticketID)
        update = dbconnect.cursor()
        update.execute(sqlUpdate)
        dbconnect.commit()
    except (pymysql.Error, pymysql.Warning) as errorMessage:
        return 0
    finally:
        dbconnect.close

def mark_ticket_as_read(ticketID):
    try:
        connect_to_database()  
        sqlUpdate = "update ticket set newMessage = 0 where ticketID = %s" %(ticketID)
        update = dbconnect.cursor()
        update.execute(sqlUpdate)
        dbconnect.commit()
    except (pymysql.Error, pymysql.Warning) as errorMessage:
        return 0
    finally:
        dbconnect.close

def update_ticket(ticketID,ticketSupporter,ticketPriority): #table with tickets
    try:
        connect_to_database()  
        sqlUpdate = "update ticket set supporterName = '%s' , ticketPriority = '%s' where ticketID = %s" %(ticketSupporter,ticketPriority,ticketID)
        update = dbconnect.cursor()
        update.execute(sqlUpdate)
        dbconnect.commit()
    except (pymysql.Error, pymysql.Warning) as errorMessage:
            return 0
    finally:
        dbconnect.close

def answer_ticket(ticketID,ticketClient,message,timestamp):
    try:
        connect_to_database()
        sqlInsert = "INSERT INTO messages (ticketID, messageFrom,messageTO, message, timestamp) VALUES(%s,'ticket@abc-logistik.com', '%s', '%s','%s')" %(ticketID,ticketClient,message,timestamp)  
        insert = dbconnect.cursor()
        insert.execute(sqlInsert)
        dbconnect.commit()
    except (pymysql.Error, pymysql.Warning) as errorMessage:
            print(errorMessage)
            return 0
    finally:
        dbconnect.close



def close_ticket_in_database(ticketID,supportInfo,closedTimeStamp):
    try:
        connect_to_database()    
        sqlUpdate = "update ticket set ticketStatus = '1', supportInfo = '%s', ticketClosed = '%s' where ticketID = %s" %(supportInfo,closedTimeStamp,ticketID)
        update = dbconnect.cursor()
        update.execute(sqlUpdate)
        dbconnect.commit()
    except (pymysql.Error, pymysql.Warning) as errorMessage:
            return 0
    finally:
        dbconnect.close

def get_all_supporter():
    try:
        connect_to_database()
        sqlQuery = "select supporterName from supporter order by supporterName asc"
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchall()
        return result
    except:
        return 0
    finally:
        dbconnect.close

def filter_ticket_database(selectedTicketID,selectedClientName,selectedDatumSpan,selectedDatumStart,selectedDatumEnd,selectedSupporter,selectedTicketStatus):
    
    if(selectedTicketID != ""):
        ticketIDquery = " ticketID = " + str(selectedTicketID)
    else:
        ticketIDquery = ""

    if(selectedClientName != ""):
        clientNameQuery = " mailadress LIKE  '%"+str(selectedClientName) + "%'"
    else:
        clientNameQuery = ""

    if(selectedDatumSpan != ""):
       
        if(selectedDatumSpan == "von/bis" and selectedDatumStart != "" and selectedDatumEnd != ""):
            if(selectedDatumStart != "" and selectedDatumEnd != ""):
                datumStartSplit = selectedDatumStart.split(".")
                selectedDatumStart = datumStartSplit[2] + "." + datumStartSplit[1] +  "." + datumStartSplit[0] 
                datumEndSplit = selectedDatumEnd.split(".")
                selectedDatumEnd = datumEndSplit[2] + "." + datumEndSplit[1] +  "." + datumEndSplit[0] 
                dateQuery = " ticketOpened between '" + str(selectedDatumStart) + " 00:00:00' and '" +str(selectedDatumEnd) + " 23:59:59'"
       
        elif (selectedDatumSpan == "von" and selectedDatumStart != ""):
                datumStartSplit = selectedDatumStart.split(".")
                selectedDatumStart = datumStartSplit[2] + "." + datumStartSplit[1] +  "." + datumStartSplit[0] 
                dateQuery = " ticketOpened >= '" + str(selectedDatumStart) + " 00:00:00'"
       
        elif (selectedDatumSpan == "bis" and selectedDatumEnd != ""):
                datumEndSplit = selectedDatumEnd.split(".")
                selectedDatumEnd = datumEndSplit[2] + "." + datumEndSplit[1] +  "." + datumEndSplit[0] 
                dateQuery = " ticketOpened <= '" + str(selectedDatumEnd) + " 00:00:00'"


        elif(selectedDatumSpan == "am" and selectedDatumStart != ""):
            datumStartSplit = selectedDatumStart.split(".")
            selectedDatumStart = datumStartSplit[2] + "." + datumStartSplit[1] +  "." + datumStartSplit[0] 
            if(selectedDatumStart != ""):
                dateQuery = " ticketOpened >= '" +str(selectedDatumStart) + "' and ticketOpened <= '" + str(selectedDatumStart) + " 23:59:59'"
        else:
            dateQuery = ""
    else:
        dateQuery = ""

    if(selectedSupporter != ""):
        supportQuery = " supporterName = '" + str(selectedSupporter) + "'"
    else:
        supportQuery = ""

    if(selectedTicketStatus != ""):
        if(selectedTicketStatus == "Abgeschlossen"):
            ticketStatusQuery = " ticketStatus = 1" 
        if(selectedTicketStatus == "Offen"):
            ticketStatusQuery = " ticketStatus = 0" 
    else:
        ticketStatusQuery = ""

    sqlQuery = "select ticketID,mailadress, header, supporterName, ticketStatus from ticket where"
    queryEnd = " order by ticketID asc"
    
    firstQueryList = [ticketIDquery,clientNameQuery,dateQuery,supportQuery,ticketStatusQuery]
    queryList = []

    for query in firstQueryList:
        if(query != ""):
            queryList.append(query)
    queryLength = len(queryList)
    if(queryLength > 1):#
        counter = 1
        for query in queryList:
            if(counter < queryLength):
                sqlQuery = str(sqlQuery) + query + " and "
            else:
                sqlQuery = str(sqlQuery) + query
            counter +=1 
    elif(queryLength == 1):
        sqlQuery = str(sqlQuery) + str(queryList[0]) + str(queryEnd)
    else:
        sqlQuery = "select ticketID,mailadress, header, supporterName, ticketStatus from ticket where ticketID >= 0 order by ticketID asc"
    try:
        connect_to_database()
        query = dbconnect.cursor()
        query.execute(sqlQuery)
        result = query.fetchall()
        return result
    except (pymysql.Error, pymysql.Warning) as errorMessage:
        print(errorMessage)
        return 0
    finally:
        dbconnect.close


def fetch_statistic():
    try:
        connect_to_database()
        statisticOverAll = []
        statisticTwoWeeks = []
        statisticYesterday = []
        statisticToday = []
        
        #tickets
        sqlTicketsOverAll = "select count(ticketID) from ticket"
        query = dbconnect.cursor()
        query.execute(sqlTicketsOverAll)
        ticketsOverAll = query.fetchone()
        statisticOverAll.append(ticketsOverAll[0])

        sqlTicketsTwoWeeks = "select count(ticketID) from ticket where ticketOpened >= CURDATE() - INTERVAL 14 DAY"
        query = dbconnect.cursor()
        query.execute(sqlTicketsTwoWeeks)
        ticketsTwoWeeks = query.fetchone()
        statisticTwoWeeks.append(ticketsTwoWeeks[0])


        sqlTicketsYesterDay = "select count(ticketID) from ticket where ticketOpened between CURDATE() - INTERVAL 1 DAY and CURDATE() - INTERVAL 0 DAY"
        query = dbconnect.cursor()
        query.execute(sqlTicketsYesterDay)
        ticketsYesterDay = query.fetchone()
        statisticYesterday.append(ticketsYesterDay[0])


        sqlTicketsToday = "select count(ticketID) from ticket WHERE ticketOpened >= CURDATE() - INTERVAL 0 DAY  "
        query = dbconnect.cursor()
        query.execute(sqlTicketsToday)
        ticketsToday = query.fetchone()
        statisticToday.append(ticketsToday[0])


        #closed#
        sqlTicketsClosedOverAll = "select count(ticketID) from ticket where ticketStatus = 1"
        query = dbconnect.cursor()
        query.execute(sqlTicketsClosedOverAll)
        ticketsClosed = query.fetchone()
        statisticOverAll.append(ticketsClosed[0])

        sqlTicketsClosedTwoWeeks = "select count(ticketID) from ticket where ticketStatus = 1 and ticketClosed >= CURDATE() - INTERVAL 14 DAY"
        query = dbconnect.cursor()
        query.execute(sqlTicketsClosedTwoWeeks)
        ticketsClosed = query.fetchone()
        statisticTwoWeeks.append(ticketsClosed[0])

        sqlTicketsClosedYesterDay = "select count(ticketID) from ticket where ticketStatus = 1 and ticketClosed between CURDATE() - INTERVAL 1 DAY and CURDATE() - INTERVAL 0 DAY"
        query = dbconnect.cursor()
        query.execute(sqlTicketsClosedYesterDay)
        ticketsClosed = query.fetchone()
        statisticYesterday.append(ticketsClosed[0])

        sqlTicketsClosedToday = "select count(ticketID) from ticket where ticketStatus = 1 and ticketClosed >= CURDATE() - INTERVAL 0 DAY  "
        query = dbconnect.cursor()
        query.execute(sqlTicketsClosedToday)
        ticketsClosed = query.fetchone()
        statisticToday.append(ticketsClosed[0])
        
        #opened
        sqlTicketsOpenOverAll = "select count(ticketID) from ticket where ticketStatus = 0"
        query = dbconnect.cursor()
        query.execute(sqlTicketsOpenOverAll)
        ticketsOpen = query.fetchone()
        statisticOverAll.append(ticketsOpen[0])

        sqlTicketsOpenTwoWeeks = "select count(ticketID) from ticket where ticketStatus = 0 and ticketOpened >= CURDATE() - INTERVAL 14 DAY"
        query = dbconnect.cursor()
        query.execute(sqlTicketsOpenTwoWeeks)
        ticketsOpen = query.fetchone()
        statisticTwoWeeks.append(ticketsOpen[0])

        sqlTicketsOpenYesterDay = "select count(ticketID) from ticket where ticketStatus = 0 and ticketOpened between CURDATE() - INTERVAL 1 DAY and CURDATE() - INTERVAL 0 DAY"
        query = dbconnect.cursor()
        query.execute(sqlTicketsOpenYesterDay)
        ticketsOpen = query.fetchone()
        statisticYesterday.append(ticketsOpen[0])

        sqlTicketsOpenToday = "select count(ticketID) from ticket where ticketStatus = 0 and ticketOpened >= CURDATE() - INTERVAL 0 DAY"
        query = dbconnect.cursor()
        query.execute(sqlTicketsOpenToday)
        ticketsOpen = query.fetchone()
        statisticToday.append(ticketsOpen[0])

        sqlSupporter = "select supporterName from supporter"
        query = dbconnect.cursor()
        query.execute(sqlSupporter)
        supporterNames = query.fetchall()

        #tickets von supportern
        for supporter in supporterNames:

            sqlTicketSupporterOverall = "select count(ticketID) from ticket where supporterName = '%s'"%(supporter)
            query = dbconnect.cursor()
            query.execute(sqlTicketSupporterOverall)
            supportOverall = query.fetchone()
            supportOverall = "Tickets bearbeitet von '" + str(supporter[0]) + "': " +str(supportOverall[0])
            statisticOverAll.append(supportOverall)

            sqlTicketSupporterTwoWeeks = "select count(ticketID) from ticket where supporterName = '%s' and ticketOpened >= CURDATE() - INTERVAL 14 DAY"%(supporter)
            query = dbconnect.cursor()
            query.execute(sqlTicketSupporterTwoWeeks)
            supportTwoWekks = query.fetchone()
            supportTwoWekks = "Tickets bearbeitet von '" + str(supporter[0]) + "': " +str(supportTwoWekks[0])
            statisticTwoWeeks.append(supportTwoWekks)

            sqlTicketSupporterYesterDay = "select count(ticketID) from ticket where supporterName = '%s' and ticketOpened between CURDATE() - INTERVAL 1 DAY and CURDATE() - INTERVAL 0 DAY"%(supporter)
            query = dbconnect.cursor()
            query.execute(sqlTicketSupporterYesterDay)
            supportYesterday = query.fetchone()
            supportYesterday = "Tickets bearbeitet von '" + str(supporter[0]) + "': " +str(supportYesterday[0])
            statisticYesterday.append(supportYesterday)

            sqlTicketSupporterToday = "select count(ticketID) from ticket where supporterName = '%s' and ticketOpened >= CURDATE() - INTERVAL 0 DAY"%(supporter)
            query = dbconnect.cursor()
            query.execute(sqlTicketSupporterToday)
            supportToday = query.fetchone()
            supportToday = "Tickets bearbeitet von '" + str(supporter[0]) + "': " +str(supportToday[0])
            statisticToday.append(supportToday)
        return statisticOverAll,statisticTwoWeeks,statisticYesterday,statisticToday
    except (pymysql.Error, pymysql.Warning) as errorMessage:
            print(errorMessage)
            return 0
    finally:
        dbconnect.close