#!/usr/bin/env python3
import os
import glob
from tkinter import ttk
from tkinter import *  
from tkinter import filedialog
from datetime import datetime
import time
import KasDatabaseGui as KasDatabaseGui
import KasDatabase as KasDatabase
import KasTicketViewGui as KasTicketViewGui
import threading
import time
import KasTicketFunctions as KasTicketFunctions
import KasTicketConfigGui as KasTicketConfigGui
import KasTicketConfigvariables as KasTicketConfigvariables
import KasTicketStatisticGui as KasTicketStatisticGui
config = KasTicketConfigvariables.get_config()

#### triggr for Ticket list update###

def trigger():
	while True:
		time.sleep(10)
		KasTicketFunctions.reload_tickets(ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab)


thread = threading.Thread(target=trigger)
thread.daemon = True
thread.start()

###############################################


window = Tk()
window.title("KasTicket")
window.configure(background='black')

tabs = ttk.Notebook(window)

actualTicketTab = Frame(tabs)
newTicketTab = Frame(tabs)
newMessageTab = Frame(tabs)

tabs.grid(row=1,column=1)


tabs.add(actualTicketTab,text="Aktuelle Tickets")
tabs.add(newTicketTab,text="Neue Tickets")	
tabs.add(newMessageTab,text="Neue Nachricht")

def open_config():
	KasTicketConfigGui.configuration_window(window)

def open_statistic():
	KasTicketStatisticGui.statistic_window(window)

def open_ticket_database():
	KasDatabaseGui.database_window(window,ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab)

def open_ticket_from_list(event):
	selectedTicket = ticketboxActualTickets.selection_get()
	KasTicketViewGui.view_ticket(window,selectedTicket,ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab)

def quit():
	window.quit()       

scrollbarActuallTickets = Scrollbar(actualTicketTab,orient='vertical',background=config[10])
scrollbarActuallTickets.grid(row=1,column=1,rowspan=3, sticky='ens')

ticketboxActualTickets = Listbox(actualTicketTab, width=60, height=10,background=config[10],highlightthickness="0",bd=0,foreground=config[9],font="bold", yscrollcommand=scrollbarActuallTickets.set)
ticketboxActualTickets.grid(row=1,column=2,rowspan=3)
ticketboxActualTickets.bind('<Double-1>', open_ticket_from_list)
scrollbarActuallTickets.config(command=ticketboxActualTickets.yview)

ButtonReloead = Button(actualTicketTab, text="Statistik",  height=3, width=15,  background='white',font="bold",command=open_statistic).grid(row=1, column=4)
ButtonDataBase = Button(actualTicketTab, text="Ticket History", height=3,width=15, background='white',font="bold",command=open_ticket_database) .grid(row=2, column=4)
ButtonConfig = Button(actualTicketTab, text="Config",height=3, width=15, background='white',font="bold",  command=open_config).grid(row=3, column=4)


scrollbarNewTickets = Scrollbar(newTicketTab,orient='vertical',background=config[10])
scrollbarNewTickets.grid(row=1,column=1,rowspan=3, sticky='ens')

ticketboxNewTickets = Listbox(newTicketTab, width=60, height=10,background=config[10],highlightthickness="0",bd=0,foreground=config[9],font="bold", yscrollcommand=scrollbarNewTickets.set)
ticketboxNewTickets.grid(row=1,column=2,rowspan=3)
ticketboxNewTickets.bind('<Double-1>', open_ticket_from_list)
scrollbarNewTickets.config(command=ticketboxNewTickets.yview)


ButtonReloead = Button(newTicketTab, text="Statistik",  height=3, width=15,  background='white',font="bold",command=open_statistic).grid(row=1, column=4)
ButtonDataBase = Button(newTicketTab, text="Ticket History", height=3,width=15, background='white',font="bold",command=open_ticket_database) .grid(row=2, column=4)
ButtonConfig = Button(newTicketTab, text="Config",height=3, width=15, background='white',font="bold",  command=open_config).grid(row=3, column=4)


scrollbarNewMessage = Scrollbar(newMessageTab,orient='vertical',background=config[10])
scrollbarNewMessage.grid(row=1,column=1,rowspan=3, sticky='ens')

ticketboxNewMessage = Listbox(newMessageTab, width=60, height=10,background=config[10],highlightthickness="0",bd=0,foreground=config[9],font="bold", yscrollcommand=scrollbarNewMessage.set)
ticketboxNewMessage.grid(row=1,column=2,rowspan=3)
ticketboxNewMessage.bind('<Double-1>', open_ticket_from_list)
scrollbarNewMessage.config(command=ticketboxNewMessage.yview)

ButtonReloead = Button(newMessageTab, text="Statistik",  height=3, width=15,  background='white',font="bold",command=open_statistic).grid(row=1, column=4)
ButtonDataBase = Button(newMessageTab, text="Ticket History", height=3,width=15, background='white',font="bold",command=open_ticket_database) .grid(row=2, column=4)
ButtonConfig = Button(newMessageTab, text="Config",height=3, width=15, background='white',font="bold",  command=open_config).grid(row=3, column=4)



KasTicketFunctions.reload_tickets(ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab)

window.resizable(False, False) 
window.mainloop()

