
import os
from tkinter import *  
from tkinter import messagebox
from tkinter.scrolledtext import *
from tkinter.simpledialog import askstring
from tkinter.messagebox import showinfo
import KasDatabase as KasDatabase
import time
import KasTicketFunctions as KasTicketFunctions
import KasTicketConfigvariables as KasTicketConfigvariables
config = KasTicketConfigvariables.get_config()

def open_database_filter(window,ticketboxAllTickets): # new window definition

    filterWindow = Toplevel(window)
    filterWindow.title("History Filter")
    filterWindow.configure(background=config[10])
    filterWindow.resizable(False, False) 

    def clear_filter():
        textTicketID.delete(0,END)
        textClientName.delete(0,END)
        dropdownDatumText.set("")
        textStartDate.delete(0,END)
        textEndDate.delete(0,END)
        dropdownSupporterText.set("")
        dropdownTicketStatusText.set("")

    def setup_filter():
        selectedTicketID = textTicketID.get()
        selectedClientName = textClientName.get()
        selectedDatumSpan = dropdownDatumText.get()
        selectedDatumStart = textStartDate.get()
        selectedDatumEnd = textEndDate.get()
        selectedSupporter = dropdownSupporterText.get()
        selectedTicketStatus = dropdownTicketStatusText.get()
        filteredTickets = KasDatabase.filter_ticket_database(selectedTicketID,selectedClientName,selectedDatumSpan,selectedDatumStart,selectedDatumEnd,selectedSupporter,selectedTicketStatus)
        KasTicketFunctions.apply_filtered_ticket_database(ticketboxAllTickets,filteredTickets)

    labelTicketID = Label(filterWindow, text="TicketID",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=10)
    labelTicketID.grid(row=1, column=2)
    textTicketID = Entry(filterWindow,width=10, font="bold")
    textTicketID.grid(row=2, column=2)

    labelClientName = Label(filterWindow, text="Client Name",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=10)
    labelClientName.grid(row=1, column=3)
    textClientName = Entry(filterWindow,width=10, font="bold")
    textClientName.grid(row=2, column=3)


    dropdownDatumFram = Frame(filterWindow)
    dropdownDatumFram.grid(row=4,column=1)
    dropdownDatumText = StringVar(filterWindow)
    dropDownDatumOptions = ["","am","von/bis","von","bis"]
    dropdownDatumText.set("") 
    dropdownDatumFram.config(bg=config[10])
    popupSupporterMenu = OptionMenu(dropdownDatumFram, dropdownDatumText, *dropDownDatumOptions)	
    popupSupporterMenu.grid(row = 4, column = 1,sticky='e')
    popupSupporterMenu.config(bg=config[10],foreground=config[9],width=5)

    labelStartDate = Label(filterWindow, text="Startdatum",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=10)
    labelStartDate.grid(row=3, column=2)
    textStartDate = Entry(filterWindow,width=10, font="bold")
    textStartDate.grid(row=4, column=2)

    labelEndDate = Label(filterWindow, text="Enddatum",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=10)
    labelEndDate.grid(row=3, column=3)
    textEndDate = Entry(filterWindow,width=10, font="bold")
    textEndDate.grid(row=4, column=3)

    labelEndDate = Label(filterWindow, text="Enddatum",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=10)
    labelEndDate.grid(row=3, column=3)
    textEndDate = Entry(filterWindow,width=10, font="bold")
    textEndDate.grid(row=4, column=3)

    labelSupporter = Label(filterWindow, text="Bearbeiter",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=10)
    labelSupporter.grid(row=5, column=2)

    dropdownSupporterFrame = Frame(filterWindow)
    dropdownSupporterFrame.grid(row=6,column=2)
    dropdownSupporterText = StringVar(filterWindow)
    dropDownSupporterOptions = KasTicketFunctions.get_supporter_names()
    dropDownSupporterOptions.insert(0,"")
    dropdownSupporterText.set("") 
    dropdownSupporterFrame.config(bg=config[10])
    popupSupporterMenu = OptionMenu(dropdownSupporterFrame, dropdownSupporterText, *dropDownSupporterOptions)	
    popupSupporterMenu.grid(row = 6, column = 2)
    popupSupporterMenu.config(bg=config[10],foreground=config[9],width=11)

    labelTicketStatus = Label(filterWindow, text="Ticket Status",anchor=W, justify=LEFT, background=config[10],foreground=config[9], font="bold", height=1, width=10)
    labelTicketStatus.grid(row=5, column=3)

    dropdownTicketStatus = Frame(filterWindow)
    dropdownTicketStatus .grid(row=6,column=3)
    dropdownTicketStatusText = StringVar(filterWindow)
    dropDownTicketStatusOption = ["","Offen","Abgeschlossen"]
    dropdownTicketStatusText.set("") 
    dropdownTicketStatus .config(bg=config[10])
    popupSupporterMenu = OptionMenu(dropdownTicketStatus , dropdownTicketStatusText, *dropDownTicketStatusOption)	
    popupSupporterMenu.grid(row = 6, column = 3)
    popupSupporterMenu.config(bg=config[10],foreground=config[9],width=11)




    freespaceLabel = Label(filterWindow, text="",height=1, width=2, background=config[10],foreground=config[9],font="bold") .grid(row=1, column=1)
    freespaceLabel = Label(filterWindow, text="",height=1, width=2, background=config[10],foreground=config[9],font="bold") .grid(row=98, column=2)
    ButtonSetClear = Button(filterWindow, text="Zurücksetzen",height=1, width=15, background="white",foreground="black",font="bold", command=clear_filter) .grid(row=99, column=2)
    ButtonSetFilter = Button(filterWindow, text="Filter setzen",height=1, width=15, background="white",foreground="black",font="bold", command=setup_filter) .grid(row=99, column=3)
    freespaceLabel = Label(filterWindow, text="",height=2, width=2, background=config[10],foreground=config[9],font="bold") .grid(row=1, column=4)
    freespaceLabel = Label(filterWindow, text="",height=2, width=2, background=config[10],foreground=config[9],font="bold") .grid(row=100, column=4)