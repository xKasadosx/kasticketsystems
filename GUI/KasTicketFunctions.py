import os
import glob
import time
from tkinter import ttk
from tkinter import *  
from tkinter import filedialog
from datetime import datetime
from tkinter.simpledialog import askstring
from tkinter.messagebox import showinfo
import KasDatabase as KasDatabase
import KasTicketConfigvariables as KasTicketConfigvariables
config = KasTicketConfigvariables.get_config()

### allround functions ###
def create_timestamp():
	timestamp = time.strftime("%Y.%m.%d %H:%M:%S")
	return timestamp


#### Main Gui###
def reload_tickets(ticketboxActualTickets,ticketboxNewTickets,ticketboxNewMessage,tabs,actualTicketTab,newTicketTab,newMessageTab):
	ticketboxActualTickets.delete(0,END)
	ticketboxNewTickets.delete(0,END)
	ticketboxNewMessage.delete(0,END)
	allOpenTickets = KasDatabase.get_all_not_done_tickets()
	for ticketInfo in allOpenTickets:
		clientName = ticketInfo[4].split("@")
		clientName = clientName[0].split(".")
		client = ""
		for part in clientName:
			client = str(part)
		if(ticketInfo[5] == True):
			ticketboxNewTickets.insert('end', "ID: " + str(ticketInfo[1]) + " | " +str(client) + " | "+ str(ticketInfo[0])+ " | "+ str(ticketInfo[2]))
		else:
			if(ticketInfo[6] == True):
				ticketboxNewMessage.insert('end', "ID: " + str(ticketInfo[1]) + " | " +str(client) +  " | "+ str(ticketInfo[0])+ " | "+ str(ticketInfo[2]))
			else:
				ticketboxActualTickets.insert('end', "ID: " + str(ticketInfo[1]) + " | " +str(client) +  " | "+ str(ticketInfo[0])+ " | "+ str(ticketInfo[2]))
	
	
	actualTickets = ticketboxActualTickets.size()
	newTickets = ticketboxNewTickets.size()
	newMessages = ticketboxNewMessage.size()

	tabs.add(actualTicketTab, text="Aktuelle Tickets ("+ str(actualTickets) + ")")
	tabs.add(newTicketTab, text="Neue Tickets (" + str(newTickets) + ")")
	tabs.add(newMessageTab, text="Neue Nachrichten (" + str(newMessages) + ")")

def format_timestamp(timestamp):
	if(timestamp == "0000-00-00 00:00:00"):
		formatTime = ""
	else:
		formatTime = timestamp.strftime("%d.%m.%Y %H:%M:%S")
	return formatTime


### Ticket View ###
def inital_ticket(ticketID,ticketContent):

	ticketInformation =  KasDatabase.get_ticket_information(ticketID)

	ticketMessages = KasDatabase.get_ticket_content(ticketID,"ticket")
	ticketAllMessages =""
	ticketContent.tag_config('ticket', background="white", foreground="black")
	for message in ticketMessages:
		messageTime = "Gesendet: " +str(message[3]) + "\n"
		messageFrom = "Von: " + str(message[1]) + "\n\n"
		header = messageTime + messageFrom
		
		ticketMessage = str(header)  + str(message[0]) + "\n"+"_"*60 + "\n"
		if("ticket@" not in message[1]):
			ticketContent.insert(1.0, ticketMessage,'ticket')
		else:
			ticketContent.insert(1.0, ticketMessage)
	ticketContent.config(state=DISABLED)
		
	ticketID = ticketInformation[0] 
	ticketClient =ticketInformation[1]
	ticketHeader = ticketInformation[2]
	ticketContentText = ticketAllMessages
	ticketSupporter = ticketInformation[4]
	ticketPriority = ticketInformation[5]
	ticketStatus = ticketInformation[6]
	ticketOpened = format_timestamp(ticketInformation[7])
	ticketClosed = format_timestamp(ticketInformation[8])
	ticketAdditionalInfo = ticketInformation[9]
		
	ticketLastMessageTime = KasDatabase.get_timestamp_of_last_message(ticketID)
	ticketLastMessageTime = ticketLastMessageTime[0]

	return ticketID,ticketClient,ticketHeader,ticketContentText,ticketSupporter,ticketPriority,ticketStatus,ticketOpened,ticketClosed,ticketAdditionalInfo,ticketLastMessageTime

def close_ticket(activeTicketWindow,ticketID):
	check = messagebox.askquestion('Ticket Abschluss','Wollen sie das Ticket schließen?')
	if(check == 'yes'):
		closedTimeStamp = create_timestamp()
		additionalTicketInfo = askstring('Abschluss Infos', 'Informationen zum Ticket')
		KasDatabase.close_ticket_in_database(ticketID,additionalTicketInfo,closedTimeStamp)
		activeTicketWindow.destroy()
	else:
		messagebox.showinfo("Abbruch","Das Ticket mit der ID: " + str(ticketID) + " wird nicht geschlossen")

        
# erneuert ticketverlauf, antwortmöglichkeit, wann letzte nachricht geschickt wurde
def reload_ticket_content(ticketAnswer,ticketContent,ticketID,labelTicketLastMessage):
	ticketAnswer.delete(1.0,END)
	ticketContent.config(state="normal")
	ticketContent.delete(1.0,END)
	
	ticketContent.tag_config('ticket', background="white", foreground="black")

	ticketMessages = KasDatabase.get_ticket_content(ticketID,"ticket")
	for message in ticketMessages:
		messageTime = "Gesendet: " +str(message[3]) + "\n"
		messageFrom = "Von: " + str(message[1]) + "\n\n"
		header = messageTime + messageFrom
		
		ticketMessage = str(header)  + str(message[0]) + "\n"+"_"*60 + "\n"
		if("ticket@" not in message[1]):
			ticketContent.insert(1.0, ticketMessage,'ticket')
		else:
			ticketContent.insert(1.0, ticketMessage)
	ticketContent.config(state=DISABLED)

	ticketLastMessageTime = KasDatabase.get_timestamp_of_last_message(ticketID)
	ticketLastMessageTime = ticketLastMessageTime[0]
	ticketInfoLastMessageTime = "Letzte Nachricht: " + str(ticketLastMessageTime)

	labelTicketLastMessage.config(text=ticketInfoLastMessageTime)


def initalize_ticket_database(ticketboxAllTickets):
	allTickets = KasDatabase.get_all_tickets()
	countAllTickets = len(allTickets)
	ticketboxAllTickets.delete(0,END)
	ticketboxAllTickets.insert('end', "------" + str(countAllTickets) + " Tickets wurden gefunden------")

	for ticketInfo in allTickets:
		clientName = ticketInfo[1].split("@")
		clientName = clientName[0].split(".")
		client = ""
		if(ticketInfo[4] == 1):
			status = str(u'\u2714')
		if(ticketInfo[4] == 0):
			status = str(u'\u2716')
		for part in clientName:
			client = str(part)
		ticketboxAllTickets.insert('end', "ID: " + str(ticketInfo[0]) + " | " +str(client) + " | "+ str(ticketInfo[3])+ " | "+ str(ticketInfo[2]) + str(status))

def apply_filtered_ticket_database(ticketboxAllTickets,filteredTickets):
	countFilteredTickets = len(filteredTickets)
	ticketboxAllTickets.delete(0,END)
	ticketboxAllTickets.insert('end', "------" + str(countFilteredTickets) + " Tickets wurden gefunden------")
	status =""
	for ticketInfo in filteredTickets:
		clientName = ticketInfo[1].split("@")
		clientName = clientName[0].split(".")
		client = ""
		if(ticketInfo[4] == 1):
			status = str(u'\u2714')
		if(ticketInfo[4] == 0):
			status = str(u'\u2716')
		for part in clientName:
			client = str(part)
		ticketboxAllTickets.insert('end', "ID: " + str(ticketInfo[0]) + " | " +str(client) + " | "+ str(ticketInfo[3])+ " | "+ str(ticketInfo[2]) + str(status))

def get_supporter_names():
	supporterList = []
	supporterNames = KasDatabase.get_all_supporter()
	for name in supporterNames:
		supporterList.append(name[0])
	return supporterList
